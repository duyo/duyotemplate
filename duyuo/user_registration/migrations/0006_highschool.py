# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2017-02-11 08:17
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('user_registration', '0005_learninginstitutions_short_name'),
    ]

    operations = [
        migrations.CreateModel(
            name='HighSchool',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200)),
                ('province', models.CharField(max_length=20)),
            ],
        ),
    ]
