from django.conf.urls import url

from . import views

app_name = 'user_registration'
urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^register$', views.register, name='register'),
    url(r'^accounts/register', views.register_user, name='register_user'),
    url(r'^accounts/login$', views.login_user, name='login_user'),
    url(r'^accounts/logout', views.logout_user, name='logout_user'),
    url(r'^accounts/profile$', views.user_profile, name='user_profile'),
    url(r'^accounts/profile/update', views.update_profile, name='update_profile'),
    url(r'^profile/education', views.user_education, name='user_education'),
    url(r'^profile/base-interests$', views.base_interests, name='base_interests'),
    url(r'^profile/base-interests/answer', views.answer_questionnaire, name='answer_questionnaire'),
    url(r'^profile/results', views.profile_results, name='profile_results'),
    url(r'^contact$', views.send_us_email, name='send_us_email'),
    url(r'^privacy', views.privacy, name='privacy'),
    url(r'^terms-of-use', views.terms, name='terms'),
    url(r'^accounts/login/visitor', views.visitor, name='visitor')
]