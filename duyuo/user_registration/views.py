from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse
from django.utils import timezone
from django.db.models import Avg
from django.template.response import TemplateResponse
from .models import Question, UserProfile, HighSchool, ExaminationType
from .models import QuestionAnswer, DomainCard, Post, Video
import logging
import operator

from user_registration.forms import (
    RegistrationForm,
    LoginForm,
    EducationForm
)
from django.contrib import auth
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserChangeForm, PasswordChangeForm
from django.contrib.auth import update_session_auth_hash, logout
from django.contrib.auth.decorators import login_required
from django.template.context import RequestContext
from django.shortcuts import render_to_response
from django.core.mail import send_mail

def index(request):
	registration_form = RegistrationForm()
	login_form = LoginForm()

	args = {'registration_form': registration_form, 'login_form': login_form}

	if request.user.is_authenticated():
		domain_cards = DomainCard.objects.all()
		posts = Post.objects.all()
		videos = Video.objects.all()
		user_has_interests = QuestionAnswer.objects.filter(user=request.user).exists()
		if user_has_interests:
			all_objects = QuestionAnswer.objects.filter(user=request.user)

			realistic_avg = all_objects.filter(question__question_group__description="Realistic").aggregate(Avg('answer'))
			investigative_avg = all_objects.filter(question__question_group__description="Investigative").aggregate(Avg('answer'))
			social_avg = all_objects.filter(question__question_group__description="Social").aggregate(Avg('answer'))
			artist_avg = all_objects.filter(question__question_group__description="Artist").aggregate(Avg('answer'))
			enterprising_avg = all_objects.filter(question__question_group__description="Enterprising").aggregate(Avg('answer'))
			conventional_avg = all_objects.filter(question__question_group__description="Conventional").aggregate(Avg('answer'))

			if all_objects:
				x = {"Realistic" : realistic_avg['answer__avg'],
						"Investigative" : investigative_avg['answer__avg'],
						"Social" : social_avg['answer__avg'],
						"Artistic" : artist_avg['answer__avg'],
						"Enterprising" : enterprising_avg['answer__avg'],
						"Conventional" : conventional_avg['answer__avg']}

				temp_results = sorted(x.items(), key=operator.itemgetter(1))
				results = temp_results[0:3] #returns 3 elements
				if results is not None:
					domain_cards = domain_cards.exclude(domain__name="Education", phase__name__in=list(dict(results).keys()))	
				
		args['cards'] = domain_cards
		args['user_has_interests'] = user_has_interests
		args['posts'] = posts
		args['videos'] = videos
	
	return render(request, 'user_registration/base.html')





	#context = RequestContext(request,{'request': request,'user': request.user,
	#	'registration_form': registration_form, 'login_form': login_form})
	#return render_to_response('user_registration/index.html',context)

def register(request):
	registration_form = RegistrationForm()

	args = {'registration_form': registration_form}
	#return TemplateResponse(request, 'user_registration/register.html', {})
	return render(request, 'user_registration/register.html', args)

	#context = RequestContext(request,{'request': request,'user': request.user,
	#	'registration_form': registration_form})
	#return render_to_response('user_registration/register.html',context)

def visitor(request):
		user = auth.authenticate(username="Visitor", password="Known321")

		if user is not None:
			if user.is_active:
				auth.login(request,user)
				return HttpResponseRedirect('/')

		args = {'registration_form': registration_form, 'login_form': login_form}
		return render(request, 'user_registration/index.html', args)

def register_user(request):
	if request.method =='POST':
		form = RegistrationForm(request.POST)
		if form.is_valid():
			form.save()

			username = form.cleaned_data['username']
			password = form.cleaned_data['password1']
			user = auth.authenticate(username=username, password=password)

			if user is not None:
				if user.is_active:
					auth.login(request,user)
					return HttpResponseRedirect('/')
		else:
			return HttpResponseRedirect('/register')
	else:
		registration_form = RegistrationForm()
		login_form = LoginForm()

		args = {'registration_form': registration_form, 'login_form': login_form}
		return render(request, 'user_registration/index.html', args)

def login_user(request):
	if request.method =='POST':
		form = LoginForm(data=request.POST)
		if form.is_valid():
			username = form.cleaned_data['username']
			password = form.cleaned_data['password']
			user = auth.authenticate(username=username, password=password)

			if user is not None:
				if user.is_active:
					auth.login(request,user)
					return HttpResponseRedirect('/')
		else:
			return HttpResponseRedirect('/')
	else:
		registration_form = RegistrationForm()
		login_form = LoginForm()

		args = {'registration_form': registration_form, 'login_form': login_form}
		return render(request, 'user_registration/index.html', args)

def logout_user(request):
	logout(request)
	return redirect(reverse('user_registration:index'))

def user_profile(request):
	high_school_data = HighSchool.objects.all().order_by('name')
	examination_type_data = ExaminationType.objects.all()

	try:
		profile_data = UserProfile.objects.get(user=request.user)
		args = {'profile_data' : profile_data}
	except :
		args = {'profile_data' : None}
	
	#if profile_data :
	#	args = {'profile_data' : profile_data}
	#else:
	#	args = {'profile_data' : None}

	args['high_school_data'] = high_school_data
	args['examination_type_data'] = examination_type_data
	return render(request, 'user_registration/create_profile.html', args)

def user_education(request):
	education_form = EducationForm()
	args = {'education_form' : education_form}
	return render(request, 'user_registration/education_questions.html', args)

def base_interests(request):
	question_list = Question.objects.all()
	args = {'question_list' : question_list}
	return render(request, 'user_registration/base_interests.html', args)

def privacy(request):
	return render(request, 'user_registration/privacy.html', {})

def terms(request):
	return render(request, 'user_registration/terms.html', {})

def update_profile(request):
	logging.warning('About to start reading form')
	if request.method =='POST':

		post_args = dict(request.POST)

		logging.warning('Form has : %s', post_args)
		current_user = User.objects.get(username=request.user.username)

		first_name = post_args['first_name'][0]
		last_name = post_args['last_name'][0]
		identity_number = post_args['identity_number'][0]
		school_name = post_args['school_name'][0]
		grade = post_args['grade'][0]
		nsc_type = post_args['nsc_type'][0]
		cell_number = post_args['cell_number'][0]
		email = post_args['email'][0]

		current_user.first_name = first_name
		current_user.last_name = last_name
		current_user.email = email

		profile_data = UserProfile.objects.update_or_create(
			user=request.user,
			defaults = {'grade' : grade, 'id_number': identity_number, 'nsc_type' : nsc_type,
					'school_name' : school_name, 'cell_number' : cell_number})

		#profile_data.user = request.user
		#profile_data.grade = grade
		#profile_data.school_name = school_name
		#profile_data.id_number =  identity_number
		#profile_data.nsc_type =  nsc_type
		#profile_data.cell_number =  cell_number

		current_user.save()
		#profile_data.save()

		profile_data = UserProfile.objects.get(user=request.user)
		args = {'profile_data' : profile_data, 'result' : "Success"}
		return render(request, 'user_registration/create_profile.html', args)
	else:
		profile_data = UserProfile.objects.get(user=request.user)
		args = {'profile_data' : profile_data, 'result' : "Failure"}
		return render(request, 'user_registration/create_profile.html', args)

def answer_questionnaire(request):
	if request.method =='POST':
		logging.warning(dict(request.POST))
		question_count = Question.objects.all().count()

		for q_id, answer_val in request.POST.items():
			if q_id.isdigit():
				key_question = Question.objects.get(pk=q_id)

				answer_data = QuestionAnswer.objects.update_or_create(
					user = request.user,
					question = key_question,
					defaults = {'answer' : answer_val}
					)
		args = {}
		return redirect(reverse('user_registration:profile_results'))
	else:
		return redirect(reverse('user_registration:index'))

def profile_results(request):
	all_objects = QuestionAnswer.objects.filter(user=request.user)

	realistic_avg = all_objects.filter(question__question_group__description="Realistic").aggregate(Avg('answer'))
	investigative_avg = all_objects.filter(question__question_group__description="Investigative").aggregate(Avg('answer'))
	social_avg = all_objects.filter(question__question_group__description="Social").aggregate(Avg('answer'))
	artist_avg = all_objects.filter(question__question_group__description="Artist").aggregate(Avg('answer'))
	enterprising_avg = all_objects.filter(question__question_group__description="Enterprising").aggregate(Avg('answer'))
	conventional_avg = all_objects.filter(question__question_group__description="Conventional").aggregate(Avg('answer'))

	if all_objects:
		x = {"Realistic" : realistic_avg['answer__avg'],
				"Investigative" : investigative_avg['answer__avg'],
				"Social" : social_avg['answer__avg'],
				"Artistic" : artist_avg['answer__avg'],
				"Enterprising" : enterprising_avg['answer__avg'],
				"Conventional" : conventional_avg['answer__avg']}

		results = sorted(x.items(), key=operator.itemgetter(1),reverse=True)
		args = {'results' : results}

		return render(request, 'user_registration/profile_results.html', args)
	else:
		args = {}
		return render(request, 'user_registration/profile_results.html', args)

def send_us_email(request):
	if request.method == 'POST':

		post_args = dict(request.POST)

		name = post_args['your_name'][0]
		subject = "Contact us e-mail from " + name
		message = post_args['message'][0]
		from_email = 'relay@eregardless.co.za'
		recipient_list = post_args['email']

		try:
			send_mail(subject=subject, message=message, from_email=from_email, recipient_list=recipient_list)
			args = {'email_sent' : "E-mail sent successfully"}
		except Exception as e:
			args = {'error' : e}

		return render(request, 'user_registration/contact.html', args)
	else:
		args = {'error' : "Something went wrong"}
		return render(request, 'user_registration/index.html', args)
