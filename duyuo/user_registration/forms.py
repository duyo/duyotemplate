from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.forms import ModelChoiceField, ModelForm
from .models import HighSchool, Question

#from user_registration.models import UserProfile


class RegistrationForm(UserCreationForm):
    email = forms.EmailField(required=False)
    password1 = forms.CharField(label='Password', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Password confirmation', widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = (
            'username',
            'email',
            #'first_name',
            #'last_name',
            'password1',
            'password2'
        )

    def clean_password2(self):
        # Check that the two password entries match
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Passwords don't match")
        return password2

    def save(self, commit=True):
        # Save the provided password in hashed format
        user = super(UserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user

class LoginForm(AuthenticationForm):
    def confirm_login_allowed(self, user):
        user = super(LoginForm, self)

        return user

class EducationForm(forms.Form):
    school = ModelChoiceField(queryset=HighSchool.objects.all().order_by('name') , empty_label="Other")
    id_number = forms.IntegerField()
    current_grade = forms.CharField(max_length=20)
    nsc_type = forms.CharField(max_length=25)