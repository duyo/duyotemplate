$(document).ready(function () {
  $('select').material_select();

  $('.dropdown-button').dropdown();


  $(".whatwedo-arrow").click(function () {
    $('html, body').animate({
      scrollTop: $("#moreonwhatwedo").offset().top
    }, 900);
  });


  $('.progress-bar').waypoint(function () {
    $('.progress-bar').css({
      animation: "animate-positive 2s",
      opacity: "1"
    });
  }, { offset: '75%' });

  $("#home-arrow-div").click(function () {
    $('html, body').animate({
      scrollTop: $("#whatwedo").offset().top
    }, 900);
  })



  $('ul:not("#faq-list")').hide();

  $('ul#faq-list').click(
    function () {
      $('div:not("#faq1")').hide();
      $(this).toggleClass('open');
    });


  // Initialize collapse button
  //$('.dropdown-button').sideNav();
  // Initialize collapsible (uncomment the line below if you use the dropdown variation)
  //$('.collapsible').collapsible();

  // Initialize collapse button
  $('.button-collapse').sideNav({
    closeOnClick: true, // Closes side-nav on <a> clicks, useful for Angular/Meteor
    draggable: true, // Choose whether you can drag to open on touch screens,
  });



  // Initialize collapsible (uncomment the line below if you use the dropdown variation)
  //$('.collapsible').collapsible();

});

//Adding sticky anchor for tabs
//ref : https://jsfiddle.net/livibetter/HV9HM/
function sticky_relocate() {
  var window_top = $(window).scrollTop();
  var div_top = $('#sticky-anchor').offset().top;
  if (window_top > div_top) {
    $('#sticky').addClass('stick');
    $('#sticky-anchor').height($('#sticky').outerHeight());
  } else {
    $('#sticky').removeClass('stick');
    $('#sticky-anchor').height(0);
  }
}

$(function () {
  $(window).scroll(sticky_relocate);
  sticky_relocate();
});

$(document).ready(function () {
  $('select').material_select();

  $('.dropdown-button').dropdown();

  $(".moreonwhatwedo-arrow").click(function () {
    $('html, body').animate({
      scrollTop: $("#casestudies").offset().top
    }, 900);
  });

  $(".casestudies-arrow").click(function () {
    $('html, body').animate({
      scrollTop: $("#teamwork").offset().top
    }, 900);
  });



  $("#teamworkdiv").mouseover(function () {
    $("#teamworkdiv").animate({ zoom: '110%' }, "slow");
  }).mouseout(function () {
    $("#teamworkdiv").animate({ zoom: '100%' }, "slow");
  });

  $(".partner8").mouseover(function () {
    $(".partner8").animate({ zoom: '110%' }, "slow");
  }).mouseout(function () {
    $(".partner8").animate({ zoom: '100%' }, "slow");
  });

  $(".partner7").mouseover(function () {
    $(".partner7").animate({ zoom: '110%' }, "slow");
  }).mouseout(function () {
    $(".partner7").animate({ zoom: '100%' }, "slow");
  });

  $(".partner6").mouseover(function () {
    $(".partner6").animate({ zoom: '110%' }, "slow");
  }).mouseout(function () {
    $(".partner6").animate({ zoom: '100%' }, "slow");
  });

  $(".partner5").mouseover(function () {
    $(".partner5").animate({ zoom: '110%' }, "slow");
  }).mouseout(function () {
    $(".partner5").animate({ zoom: '100%' }, "slow");
  });

  $(".partner4").mouseover(function () {
    $(".partner4").animate({ zoom: '110%' }, "slow");
  }).mouseout(function () {
    $(".partner4").animate({ zoom: '100%' }, "slow");
  });

  $(".partner3").mouseover(function () {
    $(".partner3").animate({ zoom: '110%' }, "slow");
  }).mouseout(function () {
    $(".partner3").animate({ zoom: '100%' }, "slow");
  });

  $(".partner2").mouseover(function () {
    $(".partner2").animate({ zoom: '110%' }, "slow");
  }).mouseout(function () {
    $(".partner2").animate({ zoom: '100%' }, "slow");
  });

  $(".partner1").mouseover(function () {
    $(".partner1").animate({ zoom: '110%' }, "slow");
  }).mouseout(function () {
    $(".partner1").animate({ zoom: '100%' }, "slow");
  });

  $(".imageBox1").mouseover(function () {
    $(".imageBox1").animate({ zoom: '110%' }, "slow");
  }).mouseout(function () {
    $(".imageBox1").animate({ zoom: '100%' }, "slow");
  });

  $(".imageBox2").mouseover(function () {
    $(".imageBox2").animate({ zoom: '110%' }, "slow");
  }).mouseout(function () {
    $(".imageBox2").animate({ zoom: '100%' }, "slow");
  });

  $(".imageBox3").mouseover(function () {
    $(".imageBox3").animate({ zoom: '110%' }, "slow");
  }).mouseout(function () {
    $(".imageBox3").animate({ zoom: '100%' }, "slow");
  });

  $(".imageBox4").mouseover(function () {
    $(".imageBox4").animate({ zoom: '110%' }, "slow");
  }).mouseout(function () {
    $(".imageBox4").animate({ zoom: '100%' }, "slow");
  });

  $(".whatwedo-arrow").click(function () {
    $('html, body').animate({
      scrollTop: $("#moreonwhatwedo").offset().top
    }, 900);
  });

  $("#casestudy2").hover(
    function () {
      $(this).filter(':not(:animated)').animate({
        marginLeft: '15px'
      }, 'slow');
      // This only fires if the row is not undergoing an animation when you mouseover it
    },
    function () {
      $(this).animate({
        marginRight: '16px'
      }, 'slow');
    });

  $("#casestudy1").hover(
    function () {
      $(this).filter(':not(:animated)').animate({
        marginLeft: '15px'
      }, 'slow');
      // This only fires if the row is not undergoing an animation when you mouseover it
    },
    function () {
      $(this).animate({
        marginLeft: '16px'
      }, 'slow');
    });

  $('#develop').hover(function () {
    $('#develop').css({
      animation: "animate-positive 2s",
      opacity: "1"
    });
  }, { offset: '75%' });

  $("#home-arrow-div").click(function () {
    $('html, body').animate({
      scrollTop: $("#whatwedo").offset().top
    }, 900);
  })


  // Initialize collapse button
  //$('.dropdown-button').sideNav();
  // Initialize collapsible (uncomment the line below if you use the dropdown variation)
  //$('.collapsible').collapsible();

  // Initialize collapse button
  $('.button-collapse').sideNav({
    closeOnClick: true, // Closes side-nav on <a> clicks, useful for Angular/Meteor
    draggable: true, // Choose whether you can drag to open on touch screens,
  });



  // Initialize collapsible (uncomment the line below if you use the dropdown variation)
  //$('.collapsible').collapsible();

});

//Adding sticky anchor for tabs
//ref : https://jsfiddle.net/livibetter/HV9HM/
function sticky_relocate() {
  var window_top = $(window).scrollTop();
  var div_top = $('#sticky-anchor').offset().top;
  if (window_top > div_top) {
    $('#sticky').addClass('stick');
    $('#sticky-anchor').height($('#sticky').outerHeight());
  } else {
    $('#sticky').removeClass('stick');
    $('#sticky-anchor').height(0);
  }
}

$(function () {
  $(window).scroll(sticky_relocate);
  sticky_relocate();
});

