from django.conf import settings
from django.db import models
import os

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

class UserProfile(models.Model):
	"""docstring for UserProfile"""
	user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True)
	school_name = models.CharField(max_length=200, null=True)	#must be dropdown
	id_number = models.CharField(null=True, max_length=20)	#must have validations
	grade = models.CharField(max_length=10, default='-1')
	age = models.IntegerField(default=0, null=True, blank=False)	#must auto calc based on ID
	nsc_type = models.CharField(null=True, max_length=50)	#must be dropdown
	cell_number = models.CharField(null=True, max_length=20)

	def __str__(self):
		return "%s %s %s" % (self.grade, self.school_name, self.age)

class Subject(models.Model):
	"""docstring for Subject"""
	subject_name = models.CharField(max_length=200)

	def __str__(self):
		return self.subject_name

		
class Mark(models.Model):
	"""docstring for Marks"""
	user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True)
	subject = models.ForeignKey(Subject, on_delete=models.CASCADE, )
	mark = models.IntegerField()

	def __str__(self):
		return "%s %s" % (self.subject.subject_name,  str(self.mark))

class QuestionGroup(models.Model):
	"""docstring for QuestionGroup"""
	"""Grouping of questions i.e Realistic, Social"""
	description = models.CharField(max_length=300)
	
	def __str__(self):
		return self.description

class QuestionType(models.Model):
	"""docstring for QuestionType"""
	"""Answer expected from question i.e YesOrNo, Options"""
	description = models.CharField(max_length=400)
	
	def __str__(self):
		return self.description

class Question(models.Model):
	"""docstring for Question"""
	question = models.CharField(max_length=500)
	question_type = models.ForeignKey(QuestionType)
	question_group = models.ForeignKey(QuestionGroup)
	
	def __str__(self):
		return "%s : %s" % (self.question_group, self.question)

class QuestionAnswer(models.Model):
	"""docstring for QuestionAnswer"""
	answer = models.IntegerField(default=0)
	user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
	question = models.ForeignKey(Question, on_delete=models.CASCADE)
	def __str__(self):
		return "%s : %s [%s]" % (self.question.question_group, self.question.question, self.answer)
		
		
class LearningInstitutions(models.Model):
	"""docstring for LearningInstitutions"""
	name = models.CharField(max_length=200)
	short_name = models.CharField(max_length=20, blank=True)

	def __str__(self):
		return "%s (%s)" % (self.name, self.short_name)

class HighSchool(models.Model):
	"""docstring for HighSchool"""
	name = models.CharField(max_length=200)
	province = models.CharField(max_length=50)

	def __str__(self):
		return "%s [%s]" % (self.name, self.province)

class ExaminationType(models.Model):
	"""docstring for ExaminationType"""
	name = models.CharField(max_length=200)
	alias = models.CharField(max_length=20, blank=True)
	
	def __str__(self):
		return "%s (%s)" % (self.name, self.alias)
		

class SystemDomain(models.Model):
	"""docstring for SystemDomains"models.Model"""
	name = models.CharField(max_length=80)
	alias = models.CharField(max_length=10, blank=True)
	description = models.CharField(max_length=400, blank=True)

	def __str__(self):
		return "%s (%s)" % (self.name, self.alias)

class Industry(models.Model):
	"""docstring for Industry"""
	name = models.CharField(max_length=80)
	alias = models.CharField(max_length=10, blank=True)
	description = models.CharField(max_length=400, blank=True)

	def __str__(self):
		return "%s (%s)" % (self.name, self.alias)

class EntPhase(models.Model):
	"""docstring for EntPhase"""
	name = models.CharField(max_length=80)
	alias = models.CharField(max_length=10, blank=True)
	description = models.CharField(max_length=400, blank=True)

	def __str__(self):
		return "%s (%s)" % (self.name, self.alias)

class ContactDetail(models.Model):
	"""docstring for ContactDetail"""
	phone_number = models.CharField(max_length=20)
	alt_number = models.CharField(max_length=20, blank=True)
	email = models.CharField(max_length=50, blank=True)
	alt_email = models.CharField(max_length=50, blank=True)
	physical_address = models.CharField(max_length=200, blank=True)
	comments = models.CharField(max_length=200, blank=True)
	url = models.CharField(max_length=150, blank=True)

	def __str__(self):
		return "%s (%s)" % (self.email, self.phone_number)


class FocusArea(models.Model):
	name = models.CharField(max_length=80)
	alias = models.CharField(max_length=10, blank=True)
	description = models.CharField(max_length=400, blank=True)

	def __str__(self):
		return "%s" % (self.name)


class DomainCard(models.Model):
	"""docstring for DomainCard"""
	name = models.CharField(max_length=100)
	description = models.CharField(max_length=600, blank=True)
	industry = models.ForeignKey(Industry)
	domain = models.ForeignKey(SystemDomain)
	phase = models.ForeignKey(EntPhase, blank=True)
	focus_1 = models.ForeignKey(FocusArea, related_name='focus_1', blank=True, null=True)
	focus_2 = models.ForeignKey(FocusArea, related_name='focus_2', blank=True, null=True)
	url = models.CharField(max_length=150, blank=True)
	contact_info = models.ForeignKey(ContactDetail, blank=True, null=True)

	def __str__(self):
		return "%s" % (self.name)

class Post(models.Model):
	"""docstring for Posts"""
	title = models.CharField(max_length=120)
	description = models.CharField(max_length=600, blank=True)
	featured = models.BooleanField(default=True)
	thumbnail = models.ImageField(upload_to = 'static/user_registration/files', max_length=500, blank=True)
	link = models.CharField(max_length=500, blank=True)
	file = models.FileField(upload_to = 'static/user_registration/files', max_length=500, blank=True)

	def __str__(self):
		return "%s" % (self.title)
		
		
	
class Video(models.Model):
	""" Videos that will be shown on duyoTV... """
	title = models.CharField(max_length=120)
	description = models.CharField(max_length=600, blank=True)
	featured = models.BooleanField(default=True)
	embed = models.CharField(max_length=500)
	
	def __str__(self):
		return "%s" % (self.title)
