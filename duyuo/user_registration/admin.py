from django.contrib import admin

from .models import UserProfile, Subject, Mark, HighSchool, ExaminationType, FocusArea
from .models import QuestionGroup, QuestionType, Question, LearningInstitutions, QuestionAnswer
from .models import SystemDomain, Industry, ContactDetail, DomainCard, EntPhase, Post, Video

admin.site.register(UserProfile)
admin.site.register(Subject)
admin.site.register(Mark)
admin.site.register(QuestionGroup)
admin.site.register(QuestionType)
admin.site.register(Question)
admin.site.register(LearningInstitutions)
admin.site.register(HighSchool)
admin.site.register(ExaminationType)
admin.site.register(QuestionAnswer)
admin.site.register(EntPhase)
admin.site.register(DomainCard)
admin.site.register(Industry)
admin.site.register(ContactDetail)
admin.site.register(SystemDomain)
admin.site.register(Post)
admin.site.register(Video)
admin.site.register(FocusArea)